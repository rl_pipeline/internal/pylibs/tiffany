cmake_minimum_required(VERSION 3.10)

cmake_policy(SET CMP0048 NEW)
project(
    tiffany
    VERSION 0.2.0
)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type" FORCE)
endif()

find_package(PkgConfig REQUIRED)

add_subdirectory(external)

include(Config)
include(StageAndInstall)

# Collect python files
file(
    GLOB_RECURSE 
    python_files 
    "${CMAKE_CURRENT_SOURCE_DIR}/python/*.py"
)

# Staging python files
set(python_version_major $ENV{PYTHON2_VERSION_MAJOR})
set(python_version_minor $ENV{PYTHON2_VERSION_MINOR})
set(python_major_minor python${python_version_major}.${python_version_minor})
set(python_build_dir ${PROJECT_NAME}/${RL_INSTALL_LIBDIR}/${python_major_minor}/site-packages)
stage_files(
    FILEPATHS
        ${python_files}
    DESTINATION
        ${python_build_dir}
)


# Collect tests files
file(
    GLOB_RECURSE 
    test_files 
    "${CMAKE_CURRENT_SOURCE_DIR}/tests/*.py"
    "${CMAKE_CURRENT_SOURCE_DIR}/tests/*.ini"
)
stage_files(
    FILEPATHS
        ${test_files}
    DESTINATION
        ${PROJECT_NAME}/tests
)


# Export pkgconfig
CONFIGURE_AND_INSTALL_PKGCONFIG(
    pkgconfig.cmake.in
    ${PROJECT_NAME}/${RL_INSTALL_LIBDIR}/pkgconfig
)


# Unit testing
enable_testing()
add_test(
    NAME tiffany_unit_test
    COMMAND 
        ${CMAKE_COMMAND} -E env 
            PYTHONPATH=${RL_STAGING_DIR}/${python_build_dir}:$ENV{PYTEST_LIBDIR}:$ENV{MOCK_LIBDIR}:$ENV{PYTHONPATH}
            PATH=$ENV{PYTEST_PREFIX}/bin:$ENV{PATH}
        $ENV{PYTEST_PREFIX}/bin/pytest -vs ${RL_STAGING_DIR}/${PROJECT_NAME}/tests/unit
)


# Install
install(
    DIRECTORY
        ${RL_STAGING_DIR}/${PROJECT_NAME}
    DESTINATION
        ${CMAKE_INSTALL_PREFIX}
    USE_SOURCE_PERMISSIONS
    PATTERN "tests" EXCLUDE
    PATTERN "*.pyc" EXCLUDE
)
