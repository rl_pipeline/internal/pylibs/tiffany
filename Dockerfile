FROM registry.gitlab.com/rl_pipeline/internal/cmake_modules:1.0.1 as cmake_modules

FROM registry.gitlab.com/rl_pipeline/internal/docker/centos-build:0.2.1 as centos-build

COPY --from=cmake_modules /usr/local/share/cmake_modules /usr/local/share/cmake_modules

ARG CMAKE_MODULE

WORKDIR /home/dev

ENV CMAKE_MODULE_PATH=${CMAKE_MODULE}:${CMAKE_MODULE_PATH}

COPY . ./tiffany

RUN python2 -m pip install -r ./tiffany/requirements.txt

RUN cd /home/dev/tiffany \
    && mkdir build \
    && cd build \
    && cmake .. \
    && ctest -V \
    && make install

FROM centos:centos8

COPY --from=centos-build /usr/local/tiffany /usr/local/tiffany

ARG UID
ARG GID
ARG USER
ARG TIFFANY_FILE_LOG_ROOT

RUN groupadd $USER -g $GID \
    && useradd -g $USER $USER -u $UID -s /bin/bash

RUN mkdir /var/log/tiffany

USER $USER

ENV PYTHONPATH=/usr/local/tiffany/lib64/python2.7/site-packages:${PYTHONPATH}
ENV TIFFANY_FILE_LOG_ROOT=TIFFANY_FILE_LOG_ROOT