# [tiffany](https://gitlab.com/rl_pipeline/internal/pylibs/tiffany.git)

### Overview
Tiffany is a configured logging module. It is a wrapper of python logging module. 
It creates a default logger object using some default handlers, filter and formatter
which will be discussed in more details below.

##### DefaultLogger
DefaultLogger is a thin wrapper of the python logging.Logger. Once instantiated,
it creates a LogRecord object which uses a structured message object with some
useful additional attributes in it. 

The LogRecord is tagged with "tiffany" attribute which is used to filter any log 
record to be emitted by the default file handler.

##### DefaultHandler & StreamFormatter
DefaultHandler inherits python logging.StreamHandler. It uses the default stream
formatter to have the prefered message structure and color, for example:

![alt text1][logo]

[logo]: images/formatting_example.jpg


##### DefaultFileHandler & DefaultFilter
DefaultFileHandler inherits python logging.handlers.TimedRotatingFileHandler.

Whenever a LogRecord is created, the logger will attach the DefaultFileHandler
to the root logger if not exists, so it is expected that all LogRecord generated
by the default logger is emitted by the DefaultFileHandler. 

The DefaultFilter is set to the DefaultFileHandler and only allows any LogRecord 
that has "tiffany" attribute.

The message is logged using JSON string formatting to the file, for example:

```json
{"uid": 1000, "relativeCreated": 41833.861112594604, "process": 23705, "tiffany": true, "module": "<ipython-input-8-733801fb2f0c>", "funcName": "<module>", "message": "hello world!", "hostname": "rickylinton-desktop", "filename": "<ipython-input-8-733801fb2f0c>", "levelno": 10, "processName": "MainProcess", "lineno": 1, "msg": {"message": "hello world!", "kwargs": {}}, "username": "rickylinton", "args": [], "euid": 1000, "exc_text": null, "name": "my_logger", "thread": 140014573668160, "created": 1591831587.498629, "threadName": "MainThread", "msecs": 498.629093170166, "pathname": "<ipython-input-8-733801fb2f0c>", "exc_info": null, "levelname": "DEBUG"}
```

We can define an environment variable **TIFFANY_FILE_LOG_ROOT** to specify the file log root directory. 
If not defined, the default is set to **/var/log/tiffany**. 

The directory structure is specified as:
```
{root}/{datetime}/{username}/{hostname}/{topcaller}/{timestamp}.log 
```

The default file logger level is set to DEBUG.

### Disclaimer
This was built and tested under:
* cmake-3.14.7
* cmake_modules-2
* docker-19.03.12
* GNU Make 4.1
* mock-3.0.5
* pytest-4.6
* python-2.7.15
* Ubuntu 18.04.3 LTS


### Prerequisites
* GNU Make
* cmake-3.14
* docker-19
* cmake_modules-1<2
* mock-3
* python-2.7
* pytest-4.6


### How to build docker image

```bash
git clone https://gitlab.com/rl_pipeline/internal/pylibs/tiffany.git
cd tiffany
git checkout tags/<tagname>
./build_dockerfile.sh [repository] [tagname] [[file_log_directory]]
```
For example:
```bash
./build_dockerfile.sh tiffany 0.1.0
./build_dockerfile.sh tiffany 0.1.0 /path/to/root/file/log/directory
```


### How to run docker image

```bash
docker run -v /etc/group:/etc/group -v /etc/shadow:/etc/shadow -v /etc/passwd:/etc/passwd -it [repository]:[tag]
```
*To make persistent log file directory, you could use volume as well*


### How to Use It
```python
import tiffany

# By default, this will create a stream and a file handler.
logger = tiffany.get_logger(__name__)

# You could remove the default root file handler by calling this method.
logger.remove_root_file_handler()

# Default stream handler level is set to WARNING. You could change the stream
# handler level by calling this method.
import logging
logger.set_stream_handler_level(logging.DEBUG)


logger.debug("Hello world, my name is {name}.", name="Foo")
logger.info("Hello world, my name is %s {surname}." % "Foo", surname="Bar")
logger.warning("Hello world, my name is {}.".format("Foo"))
logger.error("Hello world, my name is {name}.", name="Foo", surname="Bar")
logger.critical("Hello world, my name is Foo.")
```