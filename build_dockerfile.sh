#!/bin/sh

user=$(whoami)
if [ "$user" = "root" ];
then
    user="tiffany"
fi

uid=$(id -u)
if [ $uid = 0 ];
then
    uid=1000
fi

gid=$(id -u)
if [ $gid = 0 ];
then
    gid=1000
fi

docker build \
    --build-arg CMAKE_MODULE=\
/usr/local/cmake/Modules:\
/usr/local/share/cmake_modules/share/cmake_modules \
    --build-arg UID=$uid \
    --build-arg GID=$gid \
    --build-arg USER=$user\
    --build-arg TIFFANY_FILE_LOG_ROOT=${3:-/var/log/tiffany} \
    --no-cache \
    --tag $1:$2 \
    .