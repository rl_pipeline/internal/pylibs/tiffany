if(DEFINED CMakeModules_ROOT)
    set(cmake_modules_root ${CMakeModules_ROOT})
else()
    include(GNUInstallDirs)
    set(usr_local_share "/usr/local/share")
    find_path(
        cmake_modules
        NAMES cmake_modulesConfig.cmake 
        HINTS ${usr_local_share}/cmake_modules/share/cmake_modules
    )
    if(${cmake_modules} STREQUAL "cmake_modules-NOTFOUND")
        message(FATAL_ERROR "Failed to find cmake_modules in /usr/local/share!")
    else()
        set(cmake_modules_root ${usr_local_share}/cmake_modules)
    endif()
endif()

LIST(APPEND CMAKE_PREFIX_PATH ${cmake_modules_root})
find_package(
    cmake_modules 1.0.1 REQUIRED
    CONFIG
)

if(NOT cmake_modules_FOUND)
    message(FATAL_ERROR "Failed finding cmake_modules package with config.")
else()
    message(STATUS "Found cmake_modules: ${CMAKE_MODULE_PATH}(found version \"${cmake_modules_VERSION}\")")
endif()
