TIFFANY_DEFAULT_HANDLER = "TiffanyDefaultHandler"
TIFFANY_FILE_HANDLER = "TiffanyFileHandler"

LOG_RECORD_TAG = "tiffany"

DATE_TODAY_FORMAT = "%Y-%m-%d"
