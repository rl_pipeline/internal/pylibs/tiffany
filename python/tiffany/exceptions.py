class MissingRequiredEnvVar(Exception):
    pass


class MissingKeyErrorOnMessage(Exception):
    pass
