import logging


import tiffany.constants as constant


__all__ = ["DefaultFileFilter"]


class DefaultFileFilter(logging.Filter):
    def filter(self, record):
        tag = record.__dict__.get(constant.LOG_RECORD_TAG, False)
        return tag
