import copy
import logging
from contextlib import contextmanager


__all__ = [
    "AnsiColors",
    "AnsiDecorations",
    "set_level_color",
    "set_record_level_color",
    "StreamFormatter",
]


class AnsiColors:
    blue = u"\u001b[38;5;32m"
    green = u"\u001b[38;5;76m"
    yellow = u"\u001b[38;5;178m"
    orange = u"\u001b[38;5;208m"
    red = u"\u001b[38;5;196m"
    reset = u"\u001b[38;5;255m"


class AnsiDecorations:
    reset = "\033[0m"
    bold = "\033[1m"
    underline = "\033[4m"
    reversed = "\033[7m"


LEVEL_COLORS = {
    "DEBUG": AnsiColors.blue,
    "INFO": AnsiColors.green,
    "WARNING": AnsiColors.yellow,
    "WARN": AnsiColors.yellow,
    "ERROR": AnsiColors.orange,
    "CRITICAL": AnsiColors.red,
    "FATAL": AnsiColors.red,
}


_DEFAULT_FORMAT = (
    "[%(levelname)s] - %(message)s - < %(name)s, " "{bold}line %(lineno)s{reset} >"
).format(bold=AnsiDecorations.bold, reset=AnsiDecorations.reset)


def set_level_color(level, color=None):
    return "".join([color or LEVEL_COLORS[level], level, AnsiColors.reset])


@contextmanager
def set_record_level_color(record, color=None):
    default_levelname = copy.copy(record.levelname)
    record.levelname = set_level_color(record.levelname, color=color)
    yield record
    record.levelname = default_levelname


class StreamFormatter(logging.Formatter):
    def __init__(self, fmt=_DEFAULT_FORMAT, datefmt=None):
        logging.Formatter.__init__(self, fmt=fmt, datefmt=None)

    def format(self, record):
        with set_record_level_color(record) as record:
            return logging.Formatter.format(self, record)
