import collections
import json
import logging
import logging.handlers

import tiffany.formatter as formatter
import tiffany.utils as utils


__all__ = ["DefaultFileHandler", "DefaultHandler"]

_DEFAULT_FILE_HANDLER_LEVEL = logging.DEBUG
_DEFAULT_STREAM_HANDLER_LEVEL = logging.WARNING
_DEFAULT_HANDLER_FORMATTER = formatter.StreamFormatter()


class DefaultFileHandler(logging.handlers.TimedRotatingFileHandler):
    def __init__(self, backupCount=10, encoding="utf-8", interval=1, when="midnight"):
        super(DefaultFileHandler, self).__init__(
            utils.get_file_log_path(),
            backupCount=backupCount,
            encoding=encoding,
            interval=interval,
            when=when,
        )
        self.setLevel(_DEFAULT_FILE_HANDLER_LEVEL)

    def emit(self, record):
        with open(self.baseFilename, mode="a") as log_file:
            log_file.write(json.dumps(record.__dict__, cls=utils.DefaultFileEncoder))
            log_file.write("\n")


class DefaultHandler(logging.StreamHandler):
    def __init__(self):
        super(DefaultHandler, self).__init__()
        self.set_formatter(_DEFAULT_HANDLER_FORMATTER)
        self.setLevel(_DEFAULT_STREAM_HANDLER_LEVEL)

    def set_formatter(self, formatter):
        self.setFormatter(formatter)
