import getpass
import json
import logging
import os
import socket
import types

import tiffany.constants as constant
import tiffany.filter
import tiffany.handler
import tiffany.exceptions as exception
import tiffany.utils as utils


__all__ = ["DefaultLogger", "get_logger"]

_DEFAULT_LOGGER_HANDLER = None

_DEFAULT_LOGGER_LEVEL = logging.DEBUG


class DefaultLogger(logging.Logger):

    _root_handler = None

    def __init__(self, logger):
        self._logger = logger

    def __str__(self):
        return self.name

    def __getattr__(self, attr):
        if attr in self.__dict__:
            return getattr(self, attr)

        try:
            return getattr(self._logger, attr)
        except AttributeError:
            raise AttributeError(
                "{} object has no attribute {}".format(self.__class__.__name__, attr)
            )

    def _get_log_record_extras(self):
        return {
            "hostname": socket.gethostname(),
            "euid": os.geteuid(),
            "uid": os.getuid(),
            "username": getpass.getuser(),
        }

    def _add_root_file_handler(self):
        if DefaultLogger._root_handler:
            return
        handler = tiffany.handler.DefaultFileHandler()
        handler.set_name(constant.TIFFANY_FILE_HANDLER)
        handler.addFilter(tiffany.filter.DefaultFileFilter())
        logging.root.addHandler(handler)
        DefaultLogger._root_handler = handler

    def _log(self, level, msg, args, exc_info=None, extra=None, **kwargs):
        if not self.__class__._root_handler:
            self._add_root_file_handler()
        msg = utils.StructuredMessage(msg, **kwargs)
        extra = extra or {}
        extra[constant.LOG_RECORD_TAG] = True
        extra.update(self._get_log_record_extras())
        return super(DefaultLogger, self)._log(
            level, msg, args, exc_info=exc_info, extra=extra
        )

    def set_stream_handler_level(self, level):
        """Set all handlers level. Default is set to WARNING.

        Args:
            level (int)
        """
        for handler in self._logger.__dict__["handlers"]:
            handler.setLevel(level)


def get_logger(name=None):
    if not name:
        return DefaultLogger(logging.root)
    else:
        global _DEFAULT_LOGGER_HANDLER
        logger = logging.getLogger(name)
        if not _DEFAULT_LOGGER_HANDLER:
            _DEFAULT_LOGGER_HANDLER = tiffany.handler.DefaultHandler()
            _DEFAULT_LOGGER_HANDLER.set_name(constant.TIFFANY_DEFAULT_HANDLER)
        logger.addHandler(_DEFAULT_LOGGER_HANDLER)
        logger.setLevel(_DEFAULT_LOGGER_LEVEL)
        return DefaultLogger(logger)
