import ast
import datetime
import getpass
import inspect
import json
import os
import socket
import sys
import time

import tiffany.constants as constant
import tiffany.exceptions as exception


__all__ = [
    "StructuredMessage",
    "DefaultFileEncoder",
    "get_file_log_root_dir",
    "get_date_today",
    "get_top_caller_filename",
    "get_file_log_path",
    "is_debug"
]


class StructuredMessage(object):
    def __init__(self, msg, **kwargs):
        self.message = str(msg)
        self.kwargs = kwargs

    def __str__(self):
        try:
            try:
                # - For any given message that contains literals, it expects to be
                #   in the quoted or a string form so the literal_eval will work.
                #       ast.literal_eval("[1, 2]") will work.
                #       ast.literal_eval([1, 2]) will raise ValueError.
                # - For any given message that is simply string (not literals),
                #   literal_eval will raise ValueError.
                # - Python Set object doesn't work with literal_eval.
                msg = ast.literal_eval(self.message)
            except (
                SyntaxError,
                ValueError,
            ):
                # The SyntaxError is to catch a string with curly braces ({})
                try:
                    # This expects the given message to be in a literal forms.
                    ev = eval("{}".format(self.message))
                except (
                    SyntaxError,
                    NameError,
                ):
                    # This is to catch if the given message is simply a string
                    # or a string with curly braces ({}).
                    return self.message.format(**self.kwargs)
                else:
                    return self.message
            else:
                return (
                    self.message.format(**self.kwargs)
                    if isinstance(msg, str)
                    else self.message
                )
        except KeyError as e:
            raise exception.MissingKeyErrorOnMessage(e)

    def __repr__(self):
        return '{}("{}", {})'.format(
            self.__class__.__name__,
            self.message,
            ", ".join(["{}={}".format(k, v) for k, v in self.kwargs.items()]),
        )


class DefaultFileEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, StructuredMessage):
            return obj.__dict__
        else:
            return str(obj)


def get_file_log_root_dir():
    var = "TIFFANY_FILE_LOG_ROOT"
    root = os.getenv(var, default=os.path.expanduser("~"))
    if not root:
        raise exceptions.MissingRequiredEnvVar(
            "Requires setting file log root directory as {var}.".format(var)
        )
    return root


def get_date_today(date_format=constant.DATE_TODAY_FORMAT):
    return datetime.date.today().strftime(date_format)


def get_top_caller_filename():
    top_level_element = "."
    for element in inspect.stack():
        if element[1].startswith("<"):
            continue
        top_level_element = element[1]
    return os.path.splitext(os.path.basename(top_level_element))[0]


def get_file_log_path():
    try:
        log_dir = os.path.join(
            get_file_log_root_dir(),
            get_date_today(),
            getpass.getuser(),
            socket.gethostname(),
            get_top_caller_filename(),
        )
    except Exception as e:
        raise

    try:
        os.makedirs(log_dir)
    except OSError:
        pass
    finally:
        timestamp = str(time.time())
        return os.path.join(log_dir, "{}.log".format(timestamp))

    
def is_debug():
    return eval(os.getenv("TIFFANY_DEBUG", "False"))