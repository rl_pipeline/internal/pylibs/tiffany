import mock
import logging
import pytest
import os
import time

import tiffany


class DefaultLogRecord():

    def __init__(self, structured_message):
        self.msg = structured_message
        self.levelname = "WARNING"
        self.levelno = 30
        self.lineno = 1


class EditedLogRecord(DefaultLogRecord):

    def __init__(self, structured_message):
        DefaultLogRecord.__init__(self, structured_message)
        self.tiffany = True


@pytest.fixture
def name():
    return "test_logger"


@pytest.fixture
def logger(name):
    logger = tiffany.get_logger(name=name)
    return tiffany.DefaultLogger(logger)


@pytest.fixture
def root():
    return logging.root


@pytest.fixture
def default_filter():
    return tiffany.filter.DefaultFileFilter()


@pytest.fixture
def default_log_record(structured_message):
    return DefaultLogRecord(structured_message)


@pytest.fixture
def edited_log_record(structured_message):
    return EditedLogRecord(structured_message)


@pytest.fixture
def default_handler():
    return tiffany.handler.DefaultHandler()


@pytest.fixture
def file_log_path(tmpdir):
    return "{}/{}.log".format(tmpdir, time.time())


@pytest.fixture
def default_file_handler(file_log_path):
    with mock.patch(
        "tiffany.handler.utils.get_file_log_path", 
        return_value=file_log_path
    ):
        return tiffany.handler.DefaultFileHandler()


@pytest.fixture
def default_message():
    return "hello_world"


@pytest.fixture
def default_kwargs():
    return {"foo": 1, "bar": 2}


@pytest.fixture
def log_file_data():
    return {
        "msg": {"message": "hello_world", "kwargs": {"foo": 1, "bar": 2}}, 
        "levelno": 30, 
        "tiffany": True, 
        "lineno": 1, 
        "levelname": "WARNING"
    }


@pytest.fixture
def structured_message(default_message, default_kwargs):
    return tiffany.utils.StructuredMessage(default_message, **default_kwargs)


@pytest.fixture(scope="module")
def home_dir():
    return os.path.expanduser("~")