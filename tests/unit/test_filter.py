import logging

import tiffany


def test_filter_class(default_filter):
    assert type(default_filter) == tiffany.filter.DefaultFileFilter
    assert tiffany.filter.DefaultFileFilter.__bases__[0] == logging.Filter


def test_filter_default_log_record(default_filter, default_log_record):
    assert default_filter.filter(default_log_record) == False


def test_filter_edited_log_record(default_filter, edited_log_record):
    assert default_filter.filter(edited_log_record) == True
