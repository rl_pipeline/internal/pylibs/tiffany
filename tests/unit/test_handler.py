import json
import logging

import tiffany


def test_default_handler_class(default_handler):
    assert type(default_handler) == tiffany.handler.DefaultHandler
    assert default_handler.__class__.__bases__[0] == logging.StreamHandler 


def test_default_handler_formatter(default_handler):
    assert type(default_handler.formatter) == tiffany.formatter.StreamFormatter


def test_default_file_handler(default_file_handler, file_log_path):
    assert type(default_file_handler) == tiffany.handler.DefaultFileHandler
    assert default_file_handler.__class__.__bases__[0] == logging.handlers.TimedRotatingFileHandler
    assert default_file_handler.baseFilename == file_log_path


def test_default_file_handler_emit(
    default_file_handler, 
    file_log_path, 
    edited_log_record, 
    structured_message,
    log_file_data
):
    default_file_handler.emit(edited_log_record)
    with open(file_log_path, mode="r") as log_file:
        data = json.load(log_file)
        assert data == log_file_data