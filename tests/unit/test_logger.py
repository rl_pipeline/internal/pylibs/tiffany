import logging
import mock
import pytest

import tiffany


def test_logger_class(logger):
    assert type(logger) == tiffany.DefaultLogger
    assert tiffany.DefaultLogger.__bases__[0] == logging.Logger
    

def test_logger_default_settings(logger):
    assert logger.level == logging.DEBUG
    assert logger.propagate == 1


def test_logger_default_handler(logger):
    handler = logger.handlers[0]
    assert handler.level == logging.WARNING
    assert type(handler) == tiffany.handler.DefaultHandler
    assert handler.name == tiffany.constants.TIFFANY_DEFAULT_HANDLER


@mock.patch("tiffany.handler.DefaultFileHandler")
def test_logger_root_file_handler(mock_handler, logger, root):
    logger._add_root_file_handler()
    assert logger.__class__._root_handler is root.handlers[-1]


def test_logger_no_attribute_error(logger):
    with pytest.raises(AttributeError):
        logger.random_attribute()
