import json
import mock
import pytest
import os
from datetime import date

from tiffany import utils


def test_structured_message(structured_message, default_message, default_kwargs):
    assert type(structured_message) == utils.StructuredMessage
    assert str(structured_message) == default_message
    assert repr(structured_message) == "{}(\"{}\", {})".format(
        structured_message.__class__.__name__, 
        default_message, 
        ", ".join(["{}={}".format(k, v) for k, v in default_kwargs.items()])
    )
    assert "[1, 2]" == str(utils.StructuredMessage([1, 2]))
    assert "(1, 2)" == str(utils.StructuredMessage((1, 2,)))
    assert "set([1, 2])" == str(utils.StructuredMessage({1, 2,}))
    assert "{'a': 1}" == str(utils.StructuredMessage({'a': 1}))
    assert "1/2" == str(utils.StructuredMessage("{foo}/{bar}", **default_kwargs))
    assert "foo/bar" == str(utils.StructuredMessage("foo/{bar}", bar="bar"))


def test_default_file_encoder(edited_log_record, log_file_data):
    data = json.dumps(edited_log_record.__dict__, cls=utils.DefaultFileEncoder)
    assert data == json.dumps(log_file_data)


def test_get_file_log_root_dir(home_dir):
    with mock.patch.dict(
        "tiffany.utils.os.environ", {"TIFFANY_FILE_LOG_ROOT": home_dir},
        clear=True
    ):
        assert utils.get_file_log_root_dir() == home_dir


def test_get_date_today():
    with mock.patch("tiffany.utils.datetime.date") as mock_date:
        mock_date.today.return_value = date(1945, 8, 17)
        assert utils.get_date_today() == "1945-08-17"
        assert utils.get_date_today(date_format="%d-%m-%Y") == "17-08-1945"


def test_get_top_caller_filename():
    with mock.patch("tiffany.utils.inspect") as mock_inspect:
        mock_inspect.stack.return_value = [
            (None, "/usr/local/bin/python", None, None, [], None),
            (None, "<ipython-input-2-98780e2f03d8>", None, None, [], None),
            (None, "/home/user/.local/bin/ipython", None, None, [], None),
        ]
        assert utils.get_top_caller_filename() == "ipython"


@mock.patch("tiffany.utils.time.time", return_value="123456789")
@mock.patch("tiffany.utils.get_top_caller_filename", return_value="ipython")
@mock.patch("tiffany.utils.socket.gethostname", return_value="rickylinton-desktop")
@mock.patch("tiffany.utils.getpass.getuser", return_value="rlinton")
@mock.patch("tiffany.utils.get_date_today", return_value="17-08-1945")
@mock.patch("tiffany.utils.get_file_log_root_dir", return_value=os.path.expanduser("~"))
def test_get_file_log_path(home_dir, today, user, hostname, top_caller, current_time):
    with mock.patch("tiffany.utils.os.makedirs"):
        assert utils.get_file_log_path() == os.path.join(
            home_dir.return_value,
            today.return_value,
            user.return_value,
            hostname.return_value,
            top_caller.return_value,
            "{}.log".format(current_time.return_value)
        )


def test_is_debug():
    with mock.patch.dict(
        "tiffany.utils.os.environ", {"TIFFANY_DEBUG": "True"},
        clear=True
    ):
        assert utils.is_debug() == True